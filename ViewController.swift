//
//  ViewController.swift
//  WeatherApp
//
//  Created by admin on 29.01.2021.
//

import UIKit
import Alamofire
import CoreLocation


class WeatherViewController: UIViewController, CLLocationManagerDelegate, GetWeatherViewControllerDelegate {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var weatherConditionImageView: UIImageView!
    @IBOutlet weak var permissionSwitcher: UISwitch!
    @IBOutlet weak var nextButton: UIButton!
    
    public let WEATHER_URL = "https://api.openweathermap.org/data/2.5/weather"
    public let API_KEY = "a149f15270d10485bf85a023d8507b7d"
    
    var locationManager = CLLocationManager()
    
    var tempConvert = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            print("latitude:\(location.coordinate.latitude)")
            print("longitude:\(location.coordinate.longitude)")
            locationManager.stopUpdatingLocation()
            let params: [String: Any] = ["lat": location.coordinate.latitude, "lon": location.coordinate.longitude, "appid": API_KEY, "units": "metric"]
            getWeatherData(url: WEATHER_URL, params: params)
        }
    }
    
    
    
    func getWeatherData(url: String, params:[String: Any]) {
        AF.request(url, method: .get, parameters: params).responseJSON { (response) in
            switch response.result {
            case .success(let dataResponse):
                print(dataResponse)
                do{
                    
                    if let data = response.data{
                        let json = try JSONDecoder().decode(WeatherEntity.self, from: data)
                        self.updateUI(json)
                    }
                } catch{
                    print(error)
                }
            case .failure(let error):
                print(error)
                self.cityNameLabel.text = "Connection loss"
            }
            
        }
    }
    
    static var temp: Double = 0
    
    fileprivate func updateUI(_ json: WeatherEntity) {
        
        WeatherViewController.temp = json.main.temp
        temperatureLabel.text = "\(Int(WeatherViewController.temp))  C"
        
        weatherConditionImageView.image = UIImage(named: self.updateWeatherIcon(condition: json.weather[0].id))
        cityNameLabel.text = json.name
        
        
        
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        print("failed to detect position")
        cityNameLabel.text = "Location unavailable"
    }
    
    func updateWeatherIcon(condition: Int) -> String {
        switch (condition) {
        case 0...300 :
            return "tstorm1"
        case 301...500 :
            return "light_rain"
        case 501...600 :
            return "shower3"
        case 601...700 :
            return "snow4"
        case 701...771 :
            return "fog"
        case 772...799 :
            return "tstorm3"
        case 800 :
            return "sunny"
        case 801...804 :
            return "cloudy2"
        case 900...903, 905...1000 :
            return "tstorm3"
        case 903 :
            return "snow5"
        case 904 :
            return "sunny"
        default :
            return "dunno"
        }
    }
    func getWeatherForCity(with name: String) {
        let params: [String: Any] = ["q": name, "appid": API_KEY, "units": "metric"]
        getWeatherData(url: WEATHER_URL, params: params)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "getCityWeather" {
            if let destination = segue.destination as? GetWeatherViewController {
                destination.delegate = self
            }
        }
    }
    
    
    @IBAction func convertTempPressed(_ sender: UISwitch) {
        var unit: Character = "\0"
        if (!sender.isOn) {
            WeatherViewController.temp = (WeatherViewController.temp * 9/5) + 32
            unit = "F"
        } else{
            WeatherViewController.temp = (WeatherViewController.temp - 32) * 5/9
            unit = "C"
        }
        temperatureLabel.text = "\(Int(WeatherViewController.temp))  \(unit)"    }
    
}



