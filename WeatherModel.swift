//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by admin on 05.02.2021.
//

import Foundation

class WeatherEntity: Decodable {
    let weather: [Weather]
    let name: String
    let main: Main
    let sys: Sys

class Weather: Decodable {
    let id: Int
}
  
    class Main: Decodable {
        let temp:Double
    }
    class Sys: Decodable {
        let country: String
    }
  
}
