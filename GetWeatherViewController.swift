//
//  GetWeatherViewController.swift
//  WeatherApp
//
//  Created by admin on 29.01.2021.
//

import UIKit

protocol GetWeatherViewControllerDelegate: NSObjectProtocol{
    func getWeatherForCity(with name: String)
}
class GetWeatherViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet weak var getWeatherButton: UIButton!
    
   weak var delegate: GetWeatherViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getWeatherButton.layer.cornerRadius = 4
        getWeatherButton.layer.masksToBounds = true
    }

  
    @IBAction func backButtonWasPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
            
        }
    @IBAction func getWeatherButtonWasPressed(_ sender: Any) {
        let cityName: String = inputTextField.text ?? ""
        delegate?.getWeatherForCity(with: cityName)
        dismiss(animated: true, completion: nil)
        
    }
    
}

